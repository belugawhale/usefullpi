import base64
import httplib2
import json

from email.mime.text import MIMEText

from apiclient.discovery import build
from oauth2client.client import flow_from_clientsecrets
from oauth2client.file import Storage
from oauth2client.tools import run

def main(mainconfigpath, subject, message, to)

    jfile = open(mainconfigpath, "r")
    jraw = jfile.read()
    jdict = json.loads(jraw)

    g_secret = jdict["info"]["g-secret"]

    OAUTH_SCOPE = 'https://www.googleapis.com/auth/gmail.compose'

    STORAGE = Storage('gmail.storage')

    flow = flow_from_clientsecrets(g_secret, scope=OAUTH_SCOPE)
    http = httplib2.Http()

    credentials = STORAGE.get()
    if credentials is None or credentials.invalid:
      credentials = run(flow, STORAGE, http=http)
    http = credentials.authorize(http)

    gmail_service = build('gmail', 'v1', http=http)

    msg = MIMEText(message)
    msg['to'] = to
    msg['from'] = jdict["info"]["default-email-from"]
    msg['subject'] = subject
    body = {'raw': base64.b64encode(message.as_string())}

    try:
        msg = (gmail_service.users().messages().send(userId="me", body=body).execute())
        print('Message Id: %s' % msg['id'])
        jfile.close()
        return 0
    except Exception as ex:
        jfile.close()
        return ex
