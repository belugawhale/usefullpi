import json
import sys
import threading
import time

def initDevice(device, info):
    package = __import__(device["package"])
    package.main.main(device[options], info)

def main():
    jcfile = open(sys.argv[1], "r")
    jcraw = jcfile.read()
    mcfg = json.loads(jcraw)
    jcfile.close()
    for si in mcfg["server_init"]:
        sim = __import__(si)
        sim.main()
        del sim
    if mcfg["dev"] == True:
        print "This is a DEVELOPMENT enviroment."
    info = mcfg["info"]
    print mcfg["welcome"]
    print "Initiating Devices"
    jdfile = open(info["devices"])
    jdraw = jdfile.read()
    dcfg = json.loads(jdraw)
    jdfile.close()
    threads = []
    for device in dcfg:
        t = threading.Thread(target=initDevice, args=(device, info))
        threads.append(t)
        t.start()
