import RPi.GPIO as GPIO

def main(pin):
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(pin, GPIO.IN)
    return GPIO.input(pin)
