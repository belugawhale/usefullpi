import sys
sys.path.insert(0,'../..')
import server.email as emailer

def sendMail(options, info, messagge):
    if options["emails"] == "default":
        for email in info["default-emails-to"]:
            emailer.main(info["config-path"], "Your Plant %s" % options["plant-name"], message, email)
    else:
        for email in options["emails"]:
            emailer.main(info["config-path"], "Your Plant %s" % options["plant-name"], message, email)

def main(options, info):
    soil = __import__(options["input"]["soil_module"])
    try:
        timetxt = open("temp_time.txt", "r")
        timer = int(timetext.read())
        timetxt.close()
    while True:
        moisture = soil.main.main(options["input"]["pin"])
        if moisture == 1:
            timer = 0
        if moisture == 0:
            if timer == 60:
                sendMail(options, info "Please water your plant %s." % options["plant-name"])
            if timer == 480:
                sendMail(options, info "Please water your plant %s soon, it's starting to get a bit dehydrated" % options["plant-name"])
            if timer%1440 == 0 and timer < 10080:
                sendMail(options, info "Please water your plant %s, it's dehydrated" % options["plant-name"])
            if timer%10080 == 0 and timer < 43830:
                sendMail(options, info "Please water your plant %s! It's really dehydrated" % options["plant-name"])
            if timer%43830 == 0:
                sendMail(options, info "Water your plant %s now! It's super dehydrated" % options["plant-name"])
            timer = timer+1
            timetxt = open("temp_time.txt", "w")
            timetxt.write(str(timer))
            timetxt.close()
        sleep(60)
