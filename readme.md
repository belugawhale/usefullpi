**usefullpi readme**
=======

Configs
=======

Main Config
-----------

**Template File**: template.json

**Documentation**:

 - server-init - array of initialization scripts (will run main function)
 - dev - boolean stating if development enviroment or not
 - files - object containing different infos
 - welcome - ...

Devices Config
--------------

**Template File**: devices.json

**Documentation**:

 - type - type of device, Virtual: V, GPIO: G, Wireless: W
 - package - what package does this device use
 - options - package options
